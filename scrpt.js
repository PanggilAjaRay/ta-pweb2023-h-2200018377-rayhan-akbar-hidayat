// Variabel
var nama = "Yanto";
var umur = 25;

// Operator
var hasil = 10 + 5;

// Menampilkan variabel dan hasil operator
console.log("Nama: " + nama);
console.log("Umur: " + umur);
console.log("Hasil: " + hasil);

// Mengubah teks pada elemen dengan id "demo"
var element = document.getElementById("demo");
element.innerHTML = "Teks ini diubah menggunakan JavaScript!";

// Objek dengan konstruktor
function Person(nama, umur) {
  this.nama = nama;
  this.umur = umur;

  this.displayInfo = function() {
    console.log("Nama: " + this.nama);
    console.log("Umur: " + this.umur);
  }
}

// Membuat instance objek Person
var person1 = new Person("Jane", 30);
var person2 = new Person("Bob", 35);

// Memanggil method displayInfo() pada objek Person
person1.displayInfo();
person2.displayInfo();
